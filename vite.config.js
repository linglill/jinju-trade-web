import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  server: {
    proxy: {
      '/api': {//匹配所有以/api开头的路径
        target: 'http://localhost:8080',//代理目标的基础路径
        rewrite: path => path.replace(/^\/api/, ''),//路径重写，匹配api开头的字符串，并把api替换为空字符串
        
        changeOrigin: true//请求来自于。即控制请求头中host数据。默认为true说谎:来自8080;false如实回答来自代理服务器8081
      }
    }
  }
})
