import {createRouter,createWebHistory} from 'vue-router'
// 导入组件
import LoginVue from '@/views/Login.vue';
import Layoutvue from '@/views/Layout.vue';

import UserManageyVue from '@/views/user/user.vue';
import UserAvatarVue from '@/views/user/UserAvatar.vue';
import UserInfoVue from '@/views/user/UserInfo.vue';
import UserResetPasswordVue from '@/views/user/UserResetPassword.vue';
import GoodinfoVue from '@/views/Goodinfo.vue'
import ListVue from '@/views/List.vue';
import GoodVue from '@/views/Good.vue';
import shoppingcart from "@/views/shoppingcart.vue";
import MySubmitGood from "@/views/user/MySubmit.vue";

import { useUserInfoStore } from '@/store/user.js'


//定义路由关系
const routes = [
    {path:"/",component: LoginVue},
    {path:'/main',component: Layoutvue,redirect:'/Good',children:[
        
        {path:'/user/manage',component:UserManageyVue,beforeEnter:(to,from,next)=>{
            const userInfoStore = useUserInfoStore();
            const role = userInfoStore.info.role
            if(role == 0 ){
                next()
            }else{
                  alert("权限不足！")
                next('/Good')
            }
        }},

        {path:'/Good',component:GoodVue},
        {path:'/Goodinfo',component:GoodinfoVue,beforeEnter:(to,from,next)=>{
            const userInfoStore = useUserInfoStore();
            const role = userInfoStore.info.role
            if(role == 0 ){
                next()
            }else{
                alert("权限不足！")
                next('/Good')
            }
        }},
        {path:'/shoppingcart',component:shoppingcart},
        
        {path:'/user/avatar',component:UserAvatarVue},
        {path:'/user/mySubmit',component:MySubmitGood},
        {path:'/user/info',component:UserInfoVue},
        {path:'/user/resetpass',component:UserResetPasswordVue},
        {path:'/list',component:ListVue}
    ]}

]

//创建路由器
const router = createRouter({
    history:createWebHistory(),
    routes:routes
})

export default router