//  导入request.js 请求工具
import request from "@/utils/request.js";

//  查询订单
export const OrderListService = (params)=>{
    
    return  request.get('/orders/' + params );
}

// 添加订单
export async function OrderAddService(data) {
   
    // 当前面传的数据中有一个不为字符串时，用下面的方法将之转换为字符串
    const params = new URLSearchParams();
    
    for (let key in data) {
       params.append(key,data[key])
    }
    console.log(params)
   return await request.post('/orders',params);

}

//  删除订单
export async function OrderDelService(arr) {
    // 发送一个string类数据，直接在请求地址后拼接 id   接口里写成{id}这样

   return await request.delete('/orders/'+ arr);

}

// 修改订单数据
export  function OrderUpdService(params) {
    console.log('商品信息：')
    console.log(params);

    return request({
        'method': 'PUt',
        'url': '/orders',
        'params': params,
    })
}

// 订单结算
export  function PayOrderService(data) {
    console.log('结算信息：')
    console.log(data);
    //  如果后端接参总是接不到，可以这样试试
    const params = new URLSearchParams();
    
    for (let key in data) {
       params.append(key,data[key])
    }
    return  request.post('/orders/buy', params);
    
    // return request({
    //     'method': 'POST',
    //     'url': '/orders/buy',
    //     formData: params,
    // })
}

//  订单取消
export  function OrderCancelService(params) {
    
    return  request.get('/orders/cancel?id='+ params);
    
}