import request from '@/utils/request.js'

//按照搜索分页查询
export function GoodGetByPageService(params) {
    //发送异步请求，获取所有的数据
    //同步等待服务器相应的结果，并返回，async,await

    console.log(params);

    return  request.get('/goods' , { params: params });

    // 下面是给请求头带上token的尝试  成功     
    // 添加token
    // const tokenStore = useTokenStore()

    // var token =tokenStore.token;
    // console.log( token)
    // console.log( token.token)
    // return request({
    //     'method': 'GET',
    //     'url': '/goods',
    //     'params': params,
    //     'headers': {
    //         'token': token.token
    //     }
    // })
}


//获取所有待审核的货物信息(管理员)
export async function AdmGoodGetByPageService(params) {
    console.log(params)

    //  参数多的，写成下面这样，少的话就能直接写params
    return await request.get('/goods/admingoods', { params: params });

}

//文件上传
export async function GoodUploadService(formData) {
    //发送异步请求，获取所有的数据
    //同步等待服务器相应的结果，并返回，async,await
    // 发送json格式的数据，直接将参数发送过去。

    return await request.post('/upload', formData, {
        headers: {
            "Content-Type": "multipart/form-data; boundary=uploaderName"
        }
    });

    // await request({
    //     url: '/fileUpload',
    //     method: 'post', //请求方式,看后台的需求,可能是get,post等方式
    //     body: formData,
    //  })
}

// 删除商品
export async function GoodDelService(arr) {
     // 发送一个string类数据，直接在请求地址后拼接 id   接口里写成{id}这样

    return await request.delete('/goods/'+ arr);

}

// 添加商品
export async function GoodAddService(data) {
   
    // 当前面传的数据中有一个不为字符串时，用下面的方法将之转换为字符串
    const params = new URLSearchParams();
    
    for (let key in data) {
       params.append(key,data[key])
    }
    console.log(params)
   return await request.post('/goods',params);

}

// 根据用户id查询商品  用于用户查询自己上传的商品
export async function GoodSelByIdService(params) {
    
    return await request.get('/goods/pId?pId='+ params);
}

//修改商品数据
export  function GoodUpdService(params) {
    console.log('商品信息：')
    console.log(params);
    
    //  不知道为什么 这样不行。必须要下面的形式
    // return  request.put('/goods', { params: params });

    return request({
        'method': 'PUt',
        'url': '/goods',
        'params': params,
    })
}

// 商品审核
export  function GoodPassService(params) {
    
    return  request.get('/goods/pass?id='+ params);
}

//  驳回商品
export  function GoodPassFailService(params) {
    
    return  request.get('/goods/return?id='+ params);
    // return request({
    //     'method': 'GET',
    //     'url': '/goods/return',
    //     'params': params,
    // })
}