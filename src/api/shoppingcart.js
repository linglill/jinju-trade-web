//  导入request.js 请求工具
import request from "@/utils/request.js";

// 根据用户id查询商品  用于用户查询自己的购物车·
export async function GoodShoppingSelByIdService(params) {
    
    return await request.get('/shoppingCart/'+ params);

}

//修改购物车商品数据
export  function GoodShoppingUpdService(params) {
    console.log('商品信息：')
    console.log(params);
    
    //  不知道为什么 这样不行。必须要下面的形式
    // return  request.put('/goods', { params: params });

    return request({
        'method': 'PUt',
        'url': '/shoppingCart',
        'params': params,
    })
}

//批量删除购物车
export async function GoodShoppingDelService(data) {
    // 发送一个string类数据，直接在请求地址后拼接 id   接口里写成{id}这样

   return await request.delete('/shoppingCart/'+ data);

}

//添加购物车
export async function GoodShoppingAddService(data) {
   
    // 当前面传的数据中有一个不为字符串时，用下面的方法将之转换为字符串
    const params = new URLSearchParams();
    
    for (let key in data) {
       params.append(key,data[key])
    }
    console.log(params)
   return await request.post('/shoppingCart',params);

}
