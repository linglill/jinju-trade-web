//  导入request.js 请求工具
import request from "@/utils/request.js";

// 提供调用注册接口的函数



 
/////////////////////////////////////////////
//获取个人信息
export const userInfoGetService = (pId)=>{
    
    return request.get('/people/' + pId);
}

//  根据条件分页搜索
export const userListService = (params)=>{
    
    return  request.get('/people' , { params: params });
}

//  用户注册
export const userRegisterService = (registerData)=>{
    //借助于UrlSearcherParams完成传递
    // 传递  username=zhangsan&password=123456 这种样式的数据
    const params = new URLSearchParams();
    for (let key in registerData) {
       params.append(key,registerData[key])
    }
    return request.post("/people/register",params)
}

//  登录
export const userLoginService = (data) =>{
       
    const params = new URLSearchParams();
    for (let key in data) {
       params.append(key,data[key])
    }
    return request.post("/people/login",params)


     // return request({
    //     'method': 'GET',
    //     'url': '/goods',
    //     'params': params,
    //     'headers': {
    //         'token': token.token
    //     }
    // })
}

//  用户注销
//  传入用户id
export const UserDelService = (params)=>{

    return request.del("/people/ " + params)
}

//修改个人信息
export const userInfoUpdateService = (userInfo)=>{

    return request({'method': 'PUt','url': '/people','params': userInfo,})
}

// 修改密码

export const userUpdatePswService = (userInfo)=>{

    return request({'method': 'PUt','url': '/people/updatePassword','params': userInfo,})
}

//  用户充值
export const userChargeService = (info)=>{

    // return request.post('/people/addMoney', info)
    return request({'method': 'POST','url': '/people/addMoney','params': info,})
}
