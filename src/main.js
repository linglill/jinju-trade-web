import { createApp } from 'vue'
import './assets/main.scss'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css' // 导入element的样式
import App from './App.vue'
import locale from 'element-plus/es/locale/lang/zh-cn'
import router from '@/router/index'
import { createPinia } from "pinia";
import {createPersistedState} from 'pinia-persistedstate-plugin'

// import "./assets/index.css";

const app = createApp(App)   //  创建应用实例
const pinia = createPinia();
const persist = createPersistedState()
pinia.use(persist)
app.use(pinia)
app.use(router)
app.use(ElementPlus, { locale })   // 使用element-plus
app.mount('#app')  // 控制html元素 
