//   定制请求的实例
import { ElMessage } from 'element-plus'
import axios from 'axios';
// 定义一个变量，记录公共的前缀，baseURL
// const baseURL = 'http://localhost:8080';
const baseURL = '/api';
const instance = axios.create({ baseURL });

import { useTokenStore } from '@/store/token'

// 添加请求拦截器
instance.interceptors.request.use(
  (config) => {
    //请求前回调
    // 添加token
    const tokenStore = useTokenStore()
    // 判断有没有token
    if (tokenStore.token) {
      // console.log(tokenStore.token)
      // config.headers.Authorization = tokenStore.token.token
      config.headers.token = tokenStore.token.token
      
    }

    return config;
  },
  (err) => {
    //请求错误的回调
    Promise.reject(err)
  }

)
// 惰性加载
import router from '@/router'
// 添加响应拦截器
instance.interceptors.response.use(
  result => {
    // 判断业务状态码   成功的状态码
    if (result.data.code === 1) {
      return result.data;
    }

    // 操作失败
    //alert(result.data.msg?result.data.msg:'服务异常')
    ElMessage.error(result.data.msg ? result.data.msg : '服务异常');

    if (result.data.code == 0 && result.data.msg == '未登录') {
      ElMessage.error('请先登录！')
      // 返回登录页
      router.push('/')
    }

    return Promise.reject(result.data)

  },
  err => {
    // 判断响应状态码，如果位401，则证明未登录，提示请登录，并跳转到登录页面
     if (err.response.status === 401) {
     
      ElMessage.error('请先登录！')
      router.push('/')

    } else {
      ElMessage.error('服务异常')
    }

    return Promise.reject(err);// 异常的状态转化成失败的状态

  }
)

export default instance;

